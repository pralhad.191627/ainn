male(dasarath).
male(ram).
male(luv).
male(kush).
male(laxman).
male(david).

father(dasarath , ram).
father(dasarath , laxman).
father(ram , luv).
father(ram , kush).
father(luv , david).
father(laxman , sayan).
father(laxman , vikrant).
father(vikrant , koti).
father(koti , nandan).

grandfather(X ,Z):-father(X , Y) , father(Y , Z).
brother(X , Y):-father(Z, X) , father(Z , Y) , X\=Y.

uncle(X , Y):-father(Z , Y) , brother(X , Z).